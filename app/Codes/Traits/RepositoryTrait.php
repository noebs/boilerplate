<?php

namespace App\Codes\Traits;

trait RepositoryTrait
{
    public function all()
    {
        return $this->model->all();
    }

    public function insert($data)
    {
        return $this->model->create($data);
    }

    public function update($result,$data)
    {
        return $result->update($data);
    }

    public function delete($result)
    {
        return $result->delete();
    }

    public function where($filter)
    {
        return $this->model->where($filter);
    }

    public function findId($result)
    {
        return $result->value('id');
    }

    public function searchBy($result)
    {
        return $result->get();
    }

    public function findFirst($result)
    {
        return $result->first();
    }

    public function filter($data)
    {
        return array_filter($data, function($value) { return $value !== ''; });
    }

    public function totalBy($result)
    {
        return $result->count();
    }

    public function totalAll()
    {
        return $this->model->count();
    }
}
