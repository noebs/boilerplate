<?php

namespace App\Codes\Traits;

trait ValidatorTrait{

    public function passes(){

        switch ($this->flag){

            case "update":

                $this->update_rules();

                break;

            case "insert":

                $this->rules = $this->insert_rules;

                break;

            case "delete":

                $this->rules = $this->delete_rules;

                break;

            default:

                $this->rules = [];

        }

        return $this;

    }

}
