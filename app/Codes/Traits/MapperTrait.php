<?php

namespace App\Codes\Traits;

trait MapperTrait
{
    public function map($input,$flag = ''){

        switch ($flag){

            case "id":

                $this->fields = $this->id_fields;

                break;

            case "insert":

                $this->fields = $this->insert_fields;

                break;

            case "update":

                $this->fields = $this->update_fields;

                break;

            case "query":

                $this->fields = $this->query_fields;

                break;

            case "delete":

                $this->fields = $this->delete_fields;

                break;

            default:

                $this->fields = [];

        }

        return $this->iterate($input);

    }
}