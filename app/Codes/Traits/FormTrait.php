<?php

namespace App\Codes\Traits;

trait FormTrait
{
    public function all()
    {
        $response = [

            'success' => true,

            'data' => $this->repo->all(),

            'total' => $this->repo->totalAll()
        ];

        return response()->json($response,200);
    }

    public function insert($input)
    {
        $request = $this->mapper->map($input,'insert');

        if (!$this->valid($request, 'insert'))
        {
            $response['success'] = false;

            $response['message'] = $this->validator->errors();

            return response()->json($response,422);
        }

        if($this->repo->insert($request))
        {
            $response = [

                'success' => true

            ];

            return response()->json($response,200);
        }

        $response = [

            'success' => false,

            'message' => 'Create data failed'

        ];

        return response()->json($response,422);
    }

    public function update($id,$input)
    {
        $input['id'] = $id;

        $request = $this->mapper->map($input,'update');

        if (!$this->valid($request, 'update')) {

            $response['success'] = false;

            $response['message'] = $this->validator->errors();

            return response()->json($response,422);

        }

        $filter = $this->mapper->map($input,'id');

        $condition = $this->repo->where($filter);

        $result = $this->repo->findFirst($condition);

        if($this->repo->update($result,$request) == null)
        {
            $response = [

                'success' => true

            ];

            return response()->json($response,200);
        }

        $response = [

            'success' => false,

            'message' => 'Update data failed'

        ];

        return response()->json($response,422);
    }

    public function delete($id)
    {
        $input['id'] = $id;

        $request = $this->mapper->map($input, 'delete');

        if (!$this->valid($request, 'delete')) {

            $response['success'] = false;

            $response['message'] = $this->validator->errors();

            return response()->json($response,422);

        }

        $condition = $this->repo->where($request);

        $result = $this->repo->findFirst($condition);

        if($this->repo->delete($result,$request)>0)
        {
            $response = [

                'success' => true

            ];

            return response()->json($response,200);
        }

        $response = [

            'success' => false,

            'message' => 'Update data failed'

        ];

        return response()->json($response,422);

    }

    public function search($input)
    {
        $query = array();

        $model = $this->model;

        $request = $this->mapper->map($input,'query');

        $fields = $this->mapper->getSearchFields();

        foreach($fields as $value){

            $query[$value] = is_numeric($request['search']) ? intval($request['search']) : "%".$request['search']."%";

        }

        foreach ($query as $k => $v)
        {
            $model = $model->orWhere($k,'like',$v);
        }

        $response = [

            'success' => true,

            'data' => $model->get(),

            'total' => $model->count()

        ];

        return response()->json($response,200);

    }

    protected function valid(array $data, $flag)
    {
        return $this->validator->with($data, $flag)->passes()->validate();
    }
}