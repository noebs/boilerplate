<?php

namespace App\Codes\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $new_password;

    public $email;

    public function __construct($email,$new_password)
    {
        $this->email = $email;

        $this->new_password = $new_password;
    }

    public function build()
    {
        return $this->view('reset');
    }
}