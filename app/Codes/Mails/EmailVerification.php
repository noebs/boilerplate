<?php

namespace App\Codes\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    public $token;

    public $server_url;

    public function __construct($token,$server_url)
    {
        $this->token = $token;

        $this->server_url = $server_url;
    }

    public function build()
    {
        return $this->view('verify');
    }
}