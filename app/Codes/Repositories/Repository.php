<?php

namespace App\Codes\Repositories;

interface Repository{

    public function all();

    public function insert($data);

    public function update($result,$data);

    public function delete($result);

    public function where($filter);

    public function findId($result);

    public function searchBy($result);

    public function findFirst($result);

    public function filter($data);

    public function totalBy($result);

    public function totalAll();

}
