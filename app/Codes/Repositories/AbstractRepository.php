<?php

namespace App\Codes\Repositories;

use Illuminate\Database\Eloquent\Model;

use App\Codes\Traits\RepositoryTrait;

abstract class AbstractRepository implements Repository
{
    use RepositoryTrait;

    protected $model;

    public function __construct(

        Model $model

    ) {

        $this->model = $model;

    }
}
