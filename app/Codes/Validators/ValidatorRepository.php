<?php

namespace App\Codes\Validators;

interface ValidatorRepository
{
    public function with(array $data,$flag);

    public function passes();

    public function validate();

    public function errors();
}
