<?php

namespace App\Codes\Validators;

use Illuminate\Contracts\Validation\Factory;

use App\Codes\Traits\ValidatorTrait;

abstract class AbstractValidator implements ValidatorRepository
{
    use ValidatorTrait;

    protected $validator;

    protected $messages;

    protected $id;

    protected $errors;

    protected $data;

    protected $flag;

    protected $rules;

    public function __construct(

        Factory $validator

    )
    {

        $this->validator = $validator;

        $this->messages = [

            'unique' => 'Data already exists',

            'unique_with' => 'Data already exists',

        ];

    }

    public function with(array $data, $flag)
    {

        $this->flag = $flag;

        $this->data = $data;

        $this->id = array_key_exists('id', $data) ? $data['id'] : '';

        return $this;

    }

    public function validate()
    {

        $special_message = [];

        $this->errors = [];

        $validator = $this->validator->make($this->data, $this->rules, $this->messages);

        if ($validator->fails()) {

            foreach ($validator->messages()->all() as $key => $message) {

                $this->errors[] = ucfirst($message);

                if(preg_match("/exists/",$message)){

                    $special_message[$key] = $message;

                }


            }

            if(count($special_message) > 0){

                $this->errors = $special_message[0];

            }

            return false;

        }

        return true;

    }

    public function errors()
    {

        return $this->errors;

    }

    public function update_rules()
    {

        array_walk($this->update_rules, function (&$value, &$key) {

            $value = str_replace('{id}', $this->id, $value);

        });

        $this->rules = $this->update_rules;

    }
}