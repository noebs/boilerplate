<?php

use Illuminate\Filesystem\Filesystem;

function dateTimeNow()
{
    return date("Y-m-d H:i:s");
}

function dateNow()
{
    return date("Y-m-d");
}

function generateRandomNum($length = 15)
{
    return str_pad(mt_rand(0, 999999), $length, mt_rand(), STR_PAD_LEFT);
}

if(!function_exists('public_path'))
{
    function public_path($path=null)
    {
        return rtrim(app()->basePath('public/'.$path), '/');
    }
}


function storeImages($ticket_number,$image,$status,$type,$police_number = '')
{
    $data = explode(',', $image);

    $img = base64_decode($data[1]);

    $files = new Filesystem();

    if($status == 'in')
    {
        $full_path = env('UPLOAD_URL','upload').'/'.$status.'/'.dateNow().'/'.$ticket_number.'-'.$type.'.jpg';
    }
    else if($status == 'out')
    {
        $full_path = env('UPLOAD_URL','upload').'/'.$status.'/'.dateNow().'/'.$police_number.'-'.$ticket_number.'-'.$type.'.jpg';
    }

    $public_path = public_path($full_path);

    if (!$files->isDirectory(dirname($public_path))) {

        $files->makeDirectory(dirname($public_path), 0777, true, true);

    }

    $files->put($public_path,$img);

}

if ( ! function_exists('config_path'))
{
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}