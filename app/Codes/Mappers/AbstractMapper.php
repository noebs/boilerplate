<?php

namespace App\Codes\Mappers;

use App\Codes\Traits\MapperTrait;

abstract class AbstractMapper implements MapperRepository
{
    use MapperTrait;

    protected $data = [];

    protected $fields = "";

    protected $uppercase = true;

    public function getSearchFields()
    {
        return $this->search_fields;
    }

    public function iterate($input){

        $this->data = [];

        foreach($this->fields as $field)
        {
            $this->data = $this->data + ['' . $field . '' => array_key_exists($field, $input) ? !is_array($input['' . $field . '']) ? e($input['' . $field . '']) : $input['' . $field . ''] : ''];
        }

        return $this->data;

    }
}