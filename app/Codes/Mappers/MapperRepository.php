<?php

namespace App\Codes\Mappers;

interface MapperRepository
{
    public function map($input,$flag);

    public function iterate($input);

    public function getSearchFields();
}
