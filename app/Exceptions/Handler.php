<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use ErrorException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        switch ($e) {

            case $e instanceof ModelNotFoundException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message' => "Data of " . explode("\\", $e->getModel())[3] . " not found"

                ]);

                break;

            case $e instanceof MethodNotAllowedHttpException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message' => "Wrong methods"

                ],405);

                break;

            case $e instanceof NotFoundHttpException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message' => "URL not found"

                ],404);

                break;

            case $e instanceof QueryException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message' => "An error has occurred"

                ],500);

                break;

            case $e instanceof FatalErrorException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message' => "Bad request"

                ],400);

                break;


            case $e instanceof BindingResolutionException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message' => str_replace("]","",str_replace("[","",str_replace("\\"," ",$e->getMessage())))

                ],500);

                break;

            case $e instanceof \ReflectionException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message'=> str_replace("\\"," ",$e->getMessage())


                ],500);

            case $e instanceof HttpException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message' => $e->getMessage()

                ],404);

                break;

            case $e instanceof ErrorException:

                Log::debug($e);

                return response()->json([

                    'success' => false,

                    'message' => str_replace("\\"," ",$e->getMessage())

                ],500);

                break;

        }

        return parent::render($request, $e);

    }
}