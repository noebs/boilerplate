<?php

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('keys',function (){
    return str_random(32);
});